import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './componentes/home/home.component';

const routes: Routes = [

  // reglas de ruteo que se veran en pantalla:,
    {path: 'home', component: HomeComponent},
    {path: '', redirectTo: '/home' , pathMatch:'full' },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
