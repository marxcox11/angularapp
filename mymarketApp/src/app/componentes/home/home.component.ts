import { Component, AfterViewInit,Inject } from '@angular/core';
import { ApiService } from 'src/app/servicios/services.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
//import { Login } from 
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit {

 
  listarTiendas: any[] = [];
  constructor(private tienda: ApiService) {

   
  }
  ngAfterViewInit(){
    this.obtenerUsuario();
  }

  obtenerUsuario(){
    this.tienda.obtenerTienda()
    .subscribe( (data: any) =>{
      this.listarTiendas = data;
      console.log(data);
    });
  }

 
 

}
