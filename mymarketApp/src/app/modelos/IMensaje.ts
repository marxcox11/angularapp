export interface Mensaje{
    id:number;
    idUsuario:number;
    idURol:number;
    mensaje:string;
    estado:number;
    fechaCreacion:string;
}