export interface Usuario{
    id:number;
    nombres:string;
    apellidos:string;
    telefono:number;
    celular:number;
    estado:number;
    correo:string;
    contraseña:string;
}