import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Usuario } from '../modelos/IUsuario';
import { Rol } from '../modelos/IRol';
import { FotoTienda } from '../modelos/IFotoTienda';
import { Tienda } from '../modelos/ITienda';
import { UsuarioRol } from '../modelos/IUsuarioRol';
import { Mensaje } from '../modelos/IMensaje';
import { NgIf } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }
 
  //#region métodos get 
  obtenerUsuario(){
    return this.http.get("http://localhost:3000/usuario");
  }
  obtenerUsuarioRol(){
    return this.http.get("http://localhost:3000/usuariorol");
  }
  obtenerTienda(){
    return this.http.get("http://localhost:3000/tienda");
  }
  obtenerFotoTienda(){
    return this.http.get("http://localhost:3000/fotoTienda");
  }
  obtenerMensaje(){
    return this.http.get("http://localhost:3000/mensaje");
  }
  obtenerRol(){
    return this.http.get("http://localhost:3000/rol");
  }

  obtenerUsuarioXId(id:string){
    return this.http.get(`http://localhost:3000/usuario/${id}`);
  }
  obtenerUsuarioRolXId(id:string){
    return this.http.get(`http://localhost:3000/usuariorol/${id}`);
  }
  obtenerTiendaXId(id:string){
    return this.http.get(`http://localhost:3000/tienda/${id}`);
  }
  obtenerFotoTiendaXId(id:string){
    return this.http.get(`http://localhost:3000/fotoTienda/${id}`);
  }
  obtenerMensajeXId(id:string){
    return this.http.get(`http://localhost:3000/mensaje/${id}`);
  }
  obtenerRolXId(id:string){
    return this.http.get(`http://localhost:3000/rol/${id}`);
  }

  //#endregion

  //#region métodos post
  crearUsuario(usuario:Usuario){
    return this.http.post("http://localhost:3000/usuario",usuario);
  }
  crearUsuarioRol(usuarioRol:UsuarioRol){
    return this.http.post("http://localhost:3000/usuariorol",usuarioRol);
  }
  crearTienda(tienda:Tienda){
    return this.http.post("http://localhost:3000/tienda",tienda);
  }
  crearFotoTienda(fotoTienda:FotoTienda){
    return this.http.post("http://localhost:3000/fotoTienda",fotoTienda);
  }
  crearMensaje(mensaje:Mensaje){
    return this.http.post("http://localhost:3000/mensaje",mensaje);
  }
  crearRol(rol:Rol){
    return this.http.post("http://localhost:3000/usuario",rol);
  }
  //#endregion

  //#region métodos put
  actualizarUsuario(id:string,usuario:Usuario){
    return this.http.put(`http://localhost:3000/actualizarUsuario/${id}`,usuario);
  }
  actualizarUsuarioRol(id:string,usuarioRol:UsuarioRol){
    return this.http.put(`http://localhost:3000/usuariorol/${id}`,usuarioRol);
  }
  actualizarTienda(id:string,tienda:Tienda){
    return this.http.put(`http://localhost:3000/tienda/${id}`,tienda);
  }
  actualizarFotoTienda(id:string,fotoTienda:UsuarioRol){
    return this.http.put(`http://localhost:3000/fotoTienda/${id}`,fotoTienda);
  }
  actualizarRol(id:string,rol:UsuarioRol){
    return this.http.put(`http://localhost:3000/rol/${id}`,rol);
  }
  //#endregion

  //#region métodos Delete
  eliminarUsuario(id:string){
    return this.http.delete(`http://localhost:3000/usuario/${id}`);
  }
  eliminarUsuarioRol(id:string){
    return this.http.delete(`http://localhost:3000/usuariorol/${id}`);
  }
  eliminarTienda(id:string){
    return this.http.delete(`http://localhost:3000/tienda/${id}`);
  }
  eliminarFotoTienda(id:string){
    return this.http.delete(`http://localhost:3000/fotoTienda/${id}`);
  }
  eliminarMensaje(id:string){
    return this.http.delete(`http://localhost:3000/mensaje/${id}`);
  }
  eliminarRol(id:string){
    return this.http.delete(`http://localhost:3000/rol/${id}`);
  }
  //#endregion
  //#region futura api
  //#endregion  
}
